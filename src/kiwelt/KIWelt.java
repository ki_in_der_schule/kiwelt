package kiwelt;

import java.util.LinkedList;
import java.util.List;

import kiwelt.objekte.Ampel;
import kiwelt.objekte.Auto;
import kiwelt.objekte.Fußgängerampel;
import kiwelt.objekte.Katze;
import kiwelt.objekte.Person;
import kiwelt.objekte.Roboter;

public class KIWelt 
{

	public static void main( String[] args ) 
	{
		// Neues Spielfeld erzeugen
		Spielfeld spielfeld = new Spielfeld();
		spielfeld.setTitle( "Umgebung" );
		
		// Liste aller für den Roboter sichtbaren Spielobjekte erzeugen
		List<Spielobjekt> spielobjekte = new LinkedList<Spielobjekt>();
		
		// Wiese erzeugen und hinzufügen
		Spielobjekt wiese = new Spielobjekt();
		wiese.bildname = "Wiese.png";
		wiese.positionX = 0;
		wiese.positionY = 0;
		wiese.höhe = Spielfeld.höhe;
		wiese.breite = Spielfeld.breite;
		spielfeld.hinzufügen( wiese );
		
		// Straßen erzeugen
		Spielobjekt straße = new Spielobjekt();
		straße.bildname = "Straße.png";
		straße.text = "Straße";
		straße.positionX = 250;
		straße.positionY = -50;
		straße.höhe = Spielfeld.höhe + 100;
		straße.breite = Parameter.KACHEL_GRÖSSE_X * 12;
		spielfeld.hinzufügen( straße );
		spielobjekte.add( straße );
		
		// Ampel erzeugen
		boolean fußgängerAmpelGrün = false;
		boolean ampelGrün = true;
		if( Spielfeld.zufallszahl( 0, 1 ) == 1 )
		{
			fußgängerAmpelGrün = true;
			ampelGrün = false;
		}
		Ampel ampel3 = new Fußgängerampel( 435, 250 + straße.breite - Parameter.KACHEL_GRÖSSE_X * 2, 300, fußgängerAmpelGrün );
		spielfeld.hinzufügen( ampel3 );
		spielobjekte.add( ampel3 );
		Ampel ampel = new Ampel( 225, 250, 300, ampelGrün );
		spielfeld.hinzufügen( ampel );
		spielobjekte.add( ampel );
		Ampel ampel2 = new Ampel( 425, ampel.positionY + straße.breite, 300, ampelGrün );
		spielfeld.hinzufügen( ampel2 );
		spielobjekte.add( ampel2 );
		Ampel ampel4 = new Fußgängerampel( 215, ampel.positionY + ampel.höhe, 300, fußgängerAmpelGrün );
		spielfeld.hinzufügen( ampel4 );
		spielobjekte.add( ampel4 );
				

		// Autos erzeugen
		Auto auto = new Auto( straße.positionX + Parameter.KACHEL_GRÖSSE_X, straße.positionY + Spielfeld.zufallszahl( 0, 200 ) - 100, 0, 10, straße, new Ampel[]{ ampel } );
		spielfeld.hinzufügen( auto );
		spielobjekte.add( auto );
		Auto auto2 = new Auto( straße.positionX + straße.breite - auto.breite - Parameter.KACHEL_GRÖSSE_X, straße.positionY + straße.höhe  + Spielfeld.zufallszahl( 0, 200 ) - 100, 0, -10, straße, new Ampel[]{ ampel2 } );
		spielfeld.hinzufügen( auto2 );
		spielobjekte.add( auto2 );
				
		// Häuser erzeugen
		Spielobjekt haus01 = new Spielobjekt();
		haus01.bildname = "Haus.png";
		haus01.text = "Haus";
		haus01.positionX = 520;
		haus01.positionY = 450;
		haus01.höhe = Parameter.KACHEL_GRÖSSE_Y * 8;
		haus01.breite = Parameter.KACHEL_GRÖSSE_X * 8;
		spielfeld.hinzufügen( haus01 );
		spielobjekte.add( haus01 );
		Spielobjekt haus02 = new Spielobjekt();
		haus02.bildname = "Haus.png";
		haus02.text = "Haus";
		haus02.positionX = 50;
		haus02.positionY = 370;
		haus02.höhe = Parameter.KACHEL_GRÖSSE_Y * 8;
		haus02.breite = Parameter.KACHEL_GRÖSSE_X * 8;
		spielfeld.hinzufügen( haus02 );
		spielobjekte.add( haus02 );
		Spielobjekt haus03 = new Spielobjekt();
		haus03.bildname = "Haus.png";
		haus03.text = "Haus";
		haus03.positionX = 600;
		haus03.positionY = 70;
		haus03.höhe = Parameter.KACHEL_GRÖSSE_Y * 8;
		haus03.breite = Parameter.KACHEL_GRÖSSE_X * 8;
		spielfeld.hinzufügen( haus03 );
		spielobjekte.add( haus03 );
		Spielobjekt haus04 = new Spielobjekt();
		haus04.bildname = "Haus.png";
		haus04.text = "Haus";
		haus04.positionX = 600;
		haus04.positionY = 250;
		haus04.höhe = Parameter.KACHEL_GRÖSSE_Y * 8;
		haus04.breite = Parameter.KACHEL_GRÖSSE_X * 8;
		spielfeld.hinzufügen( haus04 );
		spielobjekte.add( haus04 );

		
		// Katzen erzeugen
		Katze katze = new Katze( haus02.positionX + haus02.breite, haus02.positionY + haus02.höhe, haus02 );
		spielfeld.hinzufügen( katze );
		spielobjekte.add( katze );
		Katze katze2 = new Katze( haus04.positionX - katze.breite, haus04.positionY + haus04.höhe, haus04 );
		spielfeld.hinzufügen( katze2 );
		spielobjekte.add( katze2 );
		
		// Geschäft erzeugen
		Spielobjekt geschäft = new Spielobjekt();
		geschäft.bildname = "Geschäft.png";
		geschäft.text = "Geschäft";
		geschäft.positionX = 50;
		geschäft.positionY = 70;
		geschäft.höhe = Parameter.KACHEL_GRÖSSE_Y * 8;
		geschäft.breite = Parameter.KACHEL_GRÖSSE_X * 8;
		spielfeld.hinzufügen( geschäft );
		spielobjekte.add( geschäft );
		
		// Roboter erzeugen und hinzufügen
		Roboter roboter = new Roboter( 660, 510, spielobjekte );
		spielobjekte.add( roboter );
		
		// Persoen erzeugen
		for( int zähler = 0; zähler < 8; zähler++ )
		{
			boolean berührt = true;
			Person person = null;
			do
			{
				person = new Person( Spielfeld.zufallszahl( 0, Spielfeld.breite ), Spielfeld.zufallszahl( 0, Spielfeld.höhe / 2 ), new Spielobjekt[] { straße } );
				berührt = false;
				for( Spielobjekt spielobjekt : spielobjekte )
				{
					if(    person.berührt( spielobjekt )
						|| ((person.positionX < 0) || (person.positionY < Parameter.HÖHE_FENSTER_TITELZEILE) || (person.positionX + person.breite > Spielfeld.breite) || (person.positionY + person.höhe > Spielfeld.höhe)) )
					{
						berührt = true;
						break;
					}
				}
			}
			while( berührt );
			
			spielfeld.hinzufügen( person );
			spielobjekte.add( person );
		}
		
		// Roboter ganz nach vorne setzen
		spielfeld.hinzufügen( roboter );
		
		// HAUPTSCHLEIFE
		while( true )
		{
			if( roboter.positionX < 0 || roboter.positionX + roboter.breite >= Spielfeld.breite
				|| roboter.positionY < Parameter.HÖHE_FENSTER_TITELZEILE || roboter.positionY + roboter.höhe >= Spielfeld.höhe )
			{
				System.out.println( "KOLLISION: " + "Wand" );
				
				Spielobjekt nachricht = new Spielobjekt();
				nachricht.bildname = "Kollision.png";
				nachricht.höhe = 50;
				nachricht.breite = 200;
				nachricht.positionX = Spielfeld.breite / 2 - nachricht.breite / 2;
				nachricht.positionY = Spielfeld.höhe / 2 - nachricht.höhe / 2;
				spielfeld.hinzufügen( nachricht );
				
				spielfeld.zeichnen();

				while( true )
				{
					spielfeld.warten( 1000 );
				}
			}
			
			for( Spielobjekt spielobjekt : spielobjekte )
			{
				int spielobjektVorherigePosX = spielobjekt.positionX;
				int spielobjektVorherigePosY = spielobjekt.positionY;
				
				// Spielobjekt berechnen
				spielobjekt.berechnen();

				for( Spielobjekt spielobjekt2 : spielobjekte )
				{
					if( spielobjekt.berührt( spielobjekt2 ) && spielobjekt != spielobjekt2 && !spielobjekt.text.equals( "Straße" ) && !spielobjekt2.text.equals( "Straße" ) )
					{
						// Position des Spielobjekt zurücksetzen
						spielobjekt.positionX = spielobjektVorherigePosX;
						spielobjekt.positionY = spielobjektVorherigePosY;
						
						// Wenn Kollision mit Roboter
						if( spielobjekt.text.equals( "Roboter" ) || spielobjekt2.text.equals( "Roboter" ) )
						{
							// Wenn Geschäft erreicht
							if( spielobjekt.text.equals( "Geschäft" ) || spielobjekt2.text.equals( "Geschäft" ) )
							{
								System.out.println( "AUFTRAG ERFÜLLT!" );
								
								Spielobjekt nachricht = new Spielobjekt();
								nachricht.bildname = "AufgabeErfüllt.png";
								nachricht.höhe = 50;
								nachricht.breite = 200;
								nachricht.positionX = Spielfeld.breite / 2 - nachricht.breite / 2;
								nachricht.positionY = Spielfeld.höhe / 2 - nachricht.höhe / 2;
								spielfeld.hinzufügen( nachricht );
								
								spielfeld.zeichnen();
	
								while( true )
								{
									spielfeld.warten( 1000 );
								}
							}
							
							// ...sonst KOLLISION!
							else 
							{
								if( spielobjekt.text.equals( "Roboter" ) )
								{
									System.out.println( "KOLLISION: " + spielobjekt2.text );
								}
								else
								{
									System.out.println( "KOLLISION: " + spielobjekt.text );
								}
								
								Spielobjekt nachricht = new Spielobjekt();
								nachricht.bildname = "Kollision.png";
								nachricht.höhe = 50;
								nachricht.breite = 200;
								nachricht.positionX = Spielfeld.breite / 2 - nachricht.breite / 2;
								nachricht.positionY = Spielfeld.höhe / 2 - nachricht.höhe / 2;
								spielfeld.hinzufügen( nachricht );
								
								spielfeld.zeichnen();
	
								while( true )
								{
									spielfeld.warten( 1000 );
								}
							}
						}
					}
				}
			}
			
			// Etwas warten
			spielfeld.warten( 50 );

			// Spielfeld aktualisieren
			spielfeld.zeichnen();
		}
	}
}
