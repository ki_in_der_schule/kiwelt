package kiwelt;

public class Parameter 
{
	public static final int KACHEL_GRÖSSE_X = 15;
	public static final int KACHEL_GRÖSSE_Y = 15;
	
	public static final int ROBOTER_GESCHWINDINGKEIT_X_MAX = 1;
	public static final int ROBOTER_GESCHWINDINGKEIT_Y_MAX = 1;
	
	public static final int HÖHE_FENSTER_TITELZEILE = 25;
}
