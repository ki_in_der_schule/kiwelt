package kiwelt.objekte;

import java.util.List;


/**
 * Diese Klasse dient der Steuerung des Roboters.
 * Die Steuerung des Roboters muss in der Methode mit dem Namen "berechnen" festgelegt werden.
 * Der Roboter kann dann durch Belegen der beiden Richtungsvariablen seine Bewegung festlegen. Gültige Werte sind dabei -1 (für links bzw. oben), 1 (für rechts bzw. unten), 0 (für keine Bewegung).
 * 
 */
public class RoboterSteuerung 
{
	/** Richtungsvariable für die Bewegung in x-Richtung. */
	public int richtungX;

	/** Richtungsvariable für die Bewegung in y-Richtung. */
	public int richtungY;
	
	
	/**
	 * Methode zur Steuerung des Roboters.
	 * Es werden vier Listen für die vier Sensoren des Roboters übergeben. Diese enthalten die Namen der entsprechend beschrifteten Objekte in der Umgebung des Roboters. 
	 * Der Roboter kann dann durch Belegen der beiden Richtungsvariablen seine Bewegung festlegen. Gültige Werte sind dabei -1 (für links bzw. oben), 1 (für rechts bzw. unten), 0 (für keine Bewegung).
	 * 
	 * @param sensorLinksOben    enthält die Wahrnehmungen des oberen linken Sensors in Form einer Liste von Texten
	 * @param sensorRechtsOben   enthält die Wahrnehmungen des oberen rechten Sensors in Form einer Liste von Texten
	 * @param sensorLinksUnten   enthält die Wahrnehmungen des unteren linken Sensors in Form einer Liste von Texten
	 * @param sensorRechtsUnten  enthält die Wahrnehmungen des unteren rechten Sensors in Form einer Liste von Texten
	 */
	public void berechnen( List<String> sensorLinksOben, List<String> sensorRechtsOben, List<String> sensorLinksUnten, List<String> sensorRechtsUnten )
	{
		// Hier muss die Steuerung des Roboters programmiert werden!
		// ...
	}
}
