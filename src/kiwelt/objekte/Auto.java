package kiwelt.objekte;

import kiwelt.Parameter;
import kiwelt.Spielfeld;
import kiwelt.Spielobjekt;


public class Auto extends Spielobjekt
{
	private Spielobjekt straße;
	private Ampel[] ampeln;
	
	public Auto( int startPosX, int startPosY, int richtungX, int richtungY, Spielobjekt straße )
	{
		bildname = "Auto.png";
		text = "Auto";
		breite = Parameter.KACHEL_GRÖSSE_X * 4;
		höhe = Parameter.KACHEL_GRÖSSE_Y * 4;
		positionX = startPosX;
		positionY = startPosY;
		this.richtungX = richtungX;
		this.richtungY = richtungY;
		
		this.straße = straße;
	}
	
	public Auto( int startPosX, int startPosY, int richtungX, int richtungY, Spielobjekt straße, Ampel[] ampeln )
	{
		this( startPosX, startPosY, richtungX, richtungY, straße );
		this.ampeln = ampeln; 
	}
	
	
	public void berechnen()
	{
		if( ampeln != null )
		{
			for( Ampel ampel : ampeln )
			{
				if(    Math.abs( ampel.positionX - positionX ) < straße.breite / 2 
				    && Math.abs( (ampel.positionY + ampel.höhe / 2) - (positionY + höhe / 2) ) < Math.abs( richtungY * 5 ) 
				    && ampel.text.contains( "rot" ) )
				{
					return;
				}
			}
		}
		
		positionX = positionX + richtungX;
		positionY = positionY + richtungY;
		
		if( richtungX > 0 )
		{
			if( positionX + breite > straße.positionX + straße.breite )
			{
				positionX = straße.positionX - Spielfeld.zufallszahl( 0, 100 );
			}
		}
		else if( richtungX < 0 )
		{
			if( positionX < straße.positionX )
			{
				positionX = straße.positionX + straße.breite - breite + Spielfeld.zufallszahl( 0, 100 );
			}
		}
		
		if( richtungY > 0 )
		{
			if( positionY + höhe > straße.positionY + straße.höhe )
			{
				positionY = straße.positionY - Spielfeld.zufallszahl( 0, 100 );
			}
		}
		else if( richtungY < 0 )
		{
			if( positionY < straße.positionY )
			{
				positionY = straße.positionY + straße.höhe - höhe + Spielfeld.zufallszahl( 0, 100 );
			}
		}
	}
}
