package kiwelt.objekte;

import java.util.LinkedList;
import java.util.List;

import kiwelt.Parameter;
import kiwelt.Spielfeld;
import kiwelt.Spielobjekt;


public class Roboter extends Spielobjekt
{
	public Spielobjekt sensorLinksOben;
	public Spielobjekt sensorRechtsOben;
	public Spielobjekt sensorLinksUnten;
	public Spielobjekt sensorRechtsUnten;

	private RoboterSteuerung roboterSteuerung; 
	
	private List<Spielobjekt> spielobjekte;
	

	public Roboter( int startPosX, int startPosY, List<Spielobjekt> spielobjekte )
	{
		bildname = "Roboter.png";
		höhe = Parameter.KACHEL_GRÖSSE_X * 2;
		breite = Parameter.KACHEL_GRÖSSE_Y * 2;
		positionX = startPosX; 
		positionY = startPosY;
		text = "Roboter";
		
		// Sensoren erzeugen
		sensorLinksOben = new Spielobjekt();
		sensorLinksOben.bildname = "Sensor.png";
		sensorLinksOben.text = "Sensor";
		sensorLinksOben.breite = breite * 2;
		sensorLinksOben.höhe = höhe * 2;
		sensorLinksOben.positionX = positionX - (sensorLinksOben.breite - breite / 2);
		sensorLinksOben.positionY = positionY - (sensorLinksOben.höhe - höhe / 2);
		sensorRechtsOben = new Spielobjekt();
		sensorRechtsOben.bildname = "Sensor.png";
		sensorRechtsOben.text = "      Sensor";
		sensorRechtsOben.breite = breite * 2;
		sensorRechtsOben.höhe = höhe * 2;
		sensorRechtsOben.positionX = positionX + breite / 2;
		sensorRechtsOben.positionY = positionY - (sensorRechtsOben.höhe - höhe / 2);
		sensorLinksUnten = new Spielobjekt();
		sensorLinksUnten.bildname = "Sensor.png";
		sensorLinksUnten.text = "Sensor";
		sensorLinksUnten.breite = breite * 2;
		sensorLinksUnten.höhe = höhe * 2;
		sensorLinksUnten.positionX = positionX - (sensorLinksUnten.breite - breite / 2);
		sensorLinksUnten.positionY = positionY + höhe / 2;
		sensorRechtsUnten = new Spielobjekt();
		sensorRechtsUnten.bildname = "Sensor.png";
		sensorRechtsUnten.text = "      Sensor";
		sensorRechtsUnten.breite = breite * 2;
		sensorRechtsUnten.höhe = höhe * 2;
		sensorRechtsUnten.positionX = positionX + breite / 2;
		sensorRechtsUnten.positionY = positionY + höhe / 2;
		
		// Robotersteuerung erzeugen
		roboterSteuerung = new RoboterSteuerung();
		
		this.spielobjekte = spielobjekte;
	}
	
	
	public void berechnen()
	{
		sensorLinksOben.positionX = positionX - (sensorLinksOben.breite - breite / 2);
		sensorLinksOben.positionY = positionY - (sensorLinksOben.höhe - höhe / 2);
		sensorRechtsOben.positionX = positionX + breite / 2;
		sensorRechtsOben.positionY = positionY - (sensorRechtsOben.höhe - höhe / 2);
		sensorLinksUnten.positionX = positionX - (sensorLinksUnten.breite - breite / 2);
		sensorLinksUnten.positionY = positionY + höhe / 2;
		sensorRechtsUnten.positionX = positionX + breite / 2;
		sensorRechtsUnten.positionY = positionY + höhe / 2;
		
		// Werte für Sensoren ermitteln
		List<String> berührteObjekteLinksOben = new LinkedList<String>();
		List<String> berührteObjekteRechtsOben = new LinkedList<String>();
		List<String> berührteObjekteLinksUnten = new LinkedList<String>();
		List<String> berührteObjekteRechtsUnten = new LinkedList<String>();
		for( Spielobjekt spielobjekt : spielobjekte )
		{
			if( sensorLinksOben.berührt( spielobjekt ) )
			{
				berührteObjekteLinksOben.add( spielobjekt.text );
			}
			if( sensorRechtsOben.berührt( spielobjekt ) )
			{
				berührteObjekteRechtsOben.add( spielobjekt.text );
			}
			if( sensorLinksUnten.berührt( spielobjekt ) )
			{
				berührteObjekteLinksUnten.add( spielobjekt.text );
			}
			if( sensorRechtsUnten.berührt( spielobjekt ) )
			{
				berührteObjekteRechtsUnten.add( spielobjekt.text );
			}
		}
		if( sensorLinksOben.positionX <= 0 || sensorLinksOben.positionY <= 0 )
		{
			berührteObjekteLinksOben.add( "Wand" );
		}
		if( sensorRechtsOben.positionX + sensorRechtsOben.breite >= Spielfeld.breite || sensorRechtsOben.positionY <= 0 )
		{
			berührteObjekteRechtsOben.add( "Wand" );
		}
		if( sensorLinksUnten.positionX <= 0 || sensorLinksUnten.positionY + sensorLinksUnten.höhe >= Spielfeld.höhe )
		{
			berührteObjekteLinksUnten.add( "Wand" );
		}
		if( sensorRechtsUnten.positionX + sensorRechtsUnten.breite >= Spielfeld.breite || sensorRechtsUnten.positionY + sensorRechtsUnten.höhe >= Spielfeld.höhe )
		{
			berührteObjekteRechtsUnten.add( "Wand" );
		}
		
		roboterSteuerung.berechnen( berührteObjekteLinksOben , berührteObjekteRechtsOben, berührteObjekteLinksUnten, berührteObjekteRechtsUnten );
		richtungX =  roboterSteuerung.richtungX;
		if( richtungX > Parameter.ROBOTER_GESCHWINDINGKEIT_X_MAX )
			richtungX = Parameter.ROBOTER_GESCHWINDINGKEIT_X_MAX;
		else if( richtungX < -Parameter.ROBOTER_GESCHWINDINGKEIT_X_MAX )
			richtungX = -Parameter.ROBOTER_GESCHWINDINGKEIT_X_MAX;
		richtungY =  roboterSteuerung.richtungY;
		if( richtungY > Parameter.ROBOTER_GESCHWINDINGKEIT_Y_MAX )
			richtungY = Parameter.ROBOTER_GESCHWINDINGKEIT_Y_MAX;
		else if( richtungY < -Parameter.ROBOTER_GESCHWINDINGKEIT_Y_MAX )
			richtungY = -Parameter.ROBOTER_GESCHWINDINGKEIT_Y_MAX;

		positionX = positionX + richtungX;
		positionY = positionY + richtungY;
	}
}
