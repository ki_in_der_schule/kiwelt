package kiwelt.objekte;

import kiwelt.Parameter;
import kiwelt.Spielfeld;
import kiwelt.Spielobjekt;


public class Person extends Spielobjekt
{
	private Spielobjekt straßen[];
	
	private int vorherigePosX;
	private int vorherigePosY;
	
	private int zufall;
	
	public Person( int startPosX, int startPosY, Spielobjekt[] straßen )
	{
		bildname = "Person.png";
		text = "Person";
		höhe = Parameter.KACHEL_GRÖSSE_X * 2;
		breite = Parameter.KACHEL_GRÖSSE_Y * 2;
		positionX = startPosX;
		positionY = startPosY;
		richtungX = Spielfeld.zufallszahl( -1, 1 );
		richtungY = Spielfeld.zufallszahl( -1, 1 );
		
		this.straßen = straßen;
		
		vorherigePosX = 0;
		vorherigePosY = 0;
		
		zufall = 0;
	}
	
	
	public void berechnen()
	{
		if( (vorherigePosX == positionX) && (vorherigePosY == positionY) && (zufall != 0) )
		{
			richtungX = Spielfeld.zufallszahl( -1, 1 );
			richtungY = Spielfeld.zufallszahl( -1, 1 );
		}
		
		if( Spielfeld.zufallszahl( 1, 1000 ) == 1 )
		{
			richtungX = Spielfeld.zufallszahl( -1, 1 );
		}
		if( Spielfeld.zufallszahl( 1, 1000 ) == 1 )
		{
			richtungY = Spielfeld.zufallszahl( -1, 1 );
		}
		
		vorherigePosX = positionX;		
		vorherigePosY = positionY;
		
		if( Spielfeld.zufallszahl( 1, 8 ) == 1 )
		{
			zufall = Spielfeld.zufallszahl( 0, 1 );
			positionX = positionX + richtungX * zufall;
			positionY = positionY + richtungY * zufall;
		}
		else
		{
			positionX = positionX + richtungX;
			positionY = positionY + richtungY;
		}
		
		for( Spielobjekt straße : straßen )
		{
			if(    berührt( straße )
				|| ((positionX < 0) || (positionY < Parameter.HÖHE_FENSTER_TITELZEILE) || (positionX + breite > Spielfeld.breite) || (positionY + höhe > Spielfeld.höhe)) )
			{
				positionX = vorherigePosX; 
				positionY = vorherigePosY;
				richtungX = Spielfeld.zufallszahl( -1, 1 );
				richtungY = Spielfeld.zufallszahl( -1, 1 );
				break;
			}
		}
	}
}
