package kiwelt.objekte;

import kiwelt.Parameter;
import kiwelt.Spielobjekt;


public class Ampel extends Spielobjekt
{
	private int phase;
	private int zähler;
	
	public Ampel( int positionX, int positionY, int phase, boolean istGrün )
	{
		if( istGrün )
		{
			bildname = "AmpelGrün.png";
			text = "Ampel (grün)";
		}
		else
		{
			bildname = "AmpelRot.png";
			text = "Ampel (rot)";
		}
		breite = Parameter.KACHEL_GRÖSSE_X * 2;
		höhe = Parameter.KACHEL_GRÖSSE_Y * 2;
		this.positionX = positionX;
		this.positionY = positionY;
		
		this.phase = phase;
		this.zähler = 0;
	}
	
	
	public void berechnen()
	{
		zähler = zähler + 1;
		if( zähler == phase )
		{
			zähler = 0;
			if( text.contains( "rot" ) )
			{
				bildname = "AmpelGrün.png";
				text =  text.replace( "rot", "grün" );
			}
			else
			{
				bildname = "AmpelRot.png";
				text =  text.replace( "grün", "rot" );
			}
		}
	}
}
