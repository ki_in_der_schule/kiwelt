package kiwelt.objekte;

import kiwelt.Parameter;
import kiwelt.Spielfeld;
import kiwelt.Spielobjekt;


public class Katze extends Spielobjekt 
{
	private Spielobjekt zugehörigesObjekt;
	
	
	public Katze( int startPosX, int startPosY, Spielobjekt zugehörigesObjekt )
	{
		bildname = "Katze.png";
		text = "Katze";
		höhe = Parameter.KACHEL_GRÖSSE_X;
		breite = Parameter.KACHEL_GRÖSSE_Y;
		positionX = startPosX;
		positionY = startPosY;
		richtungX = Spielfeld.zufallszahl( -1, 1 );
		richtungY = Spielfeld.zufallszahl( -1, 1 );

		this.zugehörigesObjekt = zugehörigesObjekt;
	}
	
	
	public void berechnen()
	{
		if( Spielfeld.zufallszahl( 1, 100 ) == 1 )
		{
			do
			{
				richtungX = Spielfeld.zufallszahl( -1, 1 );
				richtungY = Spielfeld.zufallszahl( -1, 1 );
			}
			while( richtungX == 0 && richtungY == 0 );
		}
				
		int vorherigePosX = positionX;
		int vorherigePosY = positionY;
		
		positionX = positionX + richtungX * Spielfeld.zufallszahl( 1 , 2 ); 
		positionY = positionY + richtungY * Spielfeld.zufallszahl( 1 , 2 );
		
		for( int zähler = 0; zähler < 3; zähler++  )
		{
			if(    !berührt( zugehörigesObjekt )
			    && !(positionX < zugehörigesObjekt.positionX - (int) (breite * 1.5)) 
				&& !(positionX > zugehörigesObjekt.positionX + zugehörigesObjekt.breite + (int) (breite * 0.5)) 
			    && !(positionY < zugehörigesObjekt.positionY - (int) (höhe * 1.5)) 
				&& !(positionY > zugehörigesObjekt.positionY + zugehörigesObjekt.höhe + (int) (höhe * 0.5)) )
			{
				break;
			}
			
			positionX = vorherigePosX; 
			positionY = vorherigePosY; 
			
			do
			{
				richtungX = Spielfeld.zufallszahl( -1, 1 );
				richtungY = Spielfeld.zufallszahl( -1, 1 );
			}
			while( richtungX == 0 && richtungY == 0 );

			positionX = positionX + richtungX * Spielfeld.zufallszahl( 1 , 2 );
			positionY = positionY + richtungY * Spielfeld.zufallszahl( 1 , 2 ); 
		}

		if(    berührt( zugehörigesObjekt )
			|| (positionX < zugehörigesObjekt.positionX - (int) (breite * 1.5)) 
			|| (positionX > zugehörigesObjekt.positionX + zugehörigesObjekt.breite + (int) (breite * 0.5)) 
			|| (positionY < zugehörigesObjekt.positionY - (int) (höhe * 1.5)) 
			|| (positionY > zugehörigesObjekt.positionY + zugehörigesObjekt.höhe + (int) (höhe * 0.5)) )
		{
			positionX = vorherigePosX; 
			positionY = vorherigePosY; 
			richtungX = 0;
			richtungY = 0;
		}
	}
}
