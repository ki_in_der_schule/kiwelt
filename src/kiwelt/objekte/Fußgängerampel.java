package kiwelt.objekte;

public class Fußgängerampel extends Ampel
{
	
	public Fußgängerampel( int positionX, int positionY, int phase, boolean istGrün )
	{
		super( positionX, positionY, phase, istGrün );
		text = text.replace( "Ampel", "Fußgängerampel" );
	}
}
